FROM ubuntu:trusty

RUN apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y install wget \
                       openjdk-7-jre \
                       firefox \
                       unzip
RUN apt-get clean

ENV USER umletuser
ENV VERSION 14.2
ENV HOME /home/${USER}
RUN useradd --create-home --home-dir ${HOME} ${USER} \
	&& chown -R ${USER}:${USER} ${HOME}

WORKDIR /opt

RUN wget -O umlet-standalone-${VERSION}.zip http://www.umlet.com/umlet_`echo $VERSION | sed 's/\./_/g'`/umlet-standalone-${VERSION}.zip
RUN unzip umlet-standalone-${VERSION}.zip
RUN rm umlet-standalone-${VERSION}.zip

RUN chown -R ${USER}:${USER} Umlet

WORKDIR ${HOME}
USER ${USER}

RUN chmod +x /opt/Umlet/umlet.sh

ENTRYPOINT /opt/Umlet/umlet.sh
