#!/bin/bash

xhost +
docker run --net=host -ti -v /tmp/.X11-unix/:/tmp/.X11-unix -v ${HOME}:/home/umletuser:rw -e DISPLAY=unix$DISPLAY umlet:latest
